# Demo
__[Heroku app](https://surecave-test.herokuapp.com)__
# How to use
* Setup python virtual environment: 
    `Python 3.7`
    `pip 9.0.1`
* Install requirements:
    ``pip install -r requirements.txt``
    
* Run server:
    - Go to root project dir
    - Run command: ``python manage.py collectstatic`` and type `yes` to confirm collects 
    - Run command: ``python manage.py runserver``

* Go to: __[localhost:8000](http://localhost:8000)__
    - Choose file: `New_York_standard-residential-lease-agreement_Blank.pdf` 
    - Select your entry point data
    - Click button `Upload`
    - After `upload` output file with filled data will shows on right sidebar
---
# How I handle
* Step 1: I use library `pdfwr` to extract fillable key from input pdf file 
by method `extract_fillable_key_pdf` in module `./pdf/utils.py`
* Step 2: I define default data in variable `new_york_residential_lease_agreement_map`,
and 2 variable with meaning `keys` for map user input, that are `new_york_residential_lease_agreement_key_map_text` for map text field 
and `new_york_residential_lease_agreement_key_map_date` for map date field
* Step 3: I use django form to get user inputs and write to the new file with default data I defined in step 2.