# Common data
days_range = range(1, 32)
months_list = [
    'January', 'February', 'March', 'April',
    'May', 'June', 'July ', 'August', 'September',
    'October', 'November', 'December'
]
years_range = range(1, 20)

# File PDF fill key map
# Key: Extract from method `extract_fillable_key_pdf`
# Value: Default value in input form
new_york_residential_lease_agreement_map = {
    "day of": {
        'values': days_range,
        'default': 15
    },
    "20": {
        'values': months_list,
        'default': months_list[2]
    },
    "hereinafter referred to as Landlord and": ["Surecave"],
    "hereinafter referred to as Tenant": ["John Doe"],
    "County": ["New York"],
    "New York such real property having a street address of":
        ["123 14th street, New York, 10001 NY"],
    "specify number of months or": ["12"],
    "and ending at 12 oclock midnight on": ["03/15/2019"],
    "undefined": ["03/15/2020"],
    "DOLLARS": ["TWO THOUSAND AND FIVE HUNDRED"],
    "undefined_2": ["2500"],
    "day of each month of the term in equal installments of": ["4"],
    "DOLLARS_2": ["TWO THOUSAND AND FIVE HUNDRED"],
    "undefined_3": ["2500"],
    "All such payments shall be made to Landlord at Landlords address as": ["03/15/2020"],
    "DOLLARS_3": ["TWO THOUSAND AND FIVE HUNDRED"],
    "receipt": ["2500"],
    "USE OF PREMISES  The Premises shall be used and occupied by Tenant and Tenant": ["John Doe"],
    "private single family dwelling and no part of the Premises shall be used at any time during the term of this": "",
    "exclusively as a": "",
    "DOLLARS_4": ["TWO THOUSAND SIX HUNDREDS"],
    "per month and except that such tenancy shall be terminable upon fifteen 15 days written": ["2600"],
    "DOLLARS_5": ["ONE HUNDRED"],
    "undefined_4": ["100"],
    "Landlords Name": ["Surecave"],
    "1": ["123 Willoughby Street, Brooklyn, NY 11201"],
    "Tenants Name": ["John Doe"],
    "1_2": ["332 54th Street, New York, NY 10080"],
    "As to Landlord this": {
        'values': days_range,
        'default': 15
    },
    "day of_2": {
        'values': months_list,
        'default': 19
    },
    "20_2": {
        'values': years_range,
        'default': 19
    },
    "As to Tenant this": {
        'values': days_range,
        'default': 15
    },
    "day of_3": {
        'values': months_list,
        'default': months_list[2]
    },
    "20_3": {
        'values': years_range,
        'default': 19
    }
}

# Key: meaning key for user input form
# Value: fillable field key in PDF file
new_york_residential_lease_agreement_key_map_text = {
    "landlord": "hereinafter referred to as Landlord and",
    "tenant": "hereinafter referred to as Tenant",
    "city": "County",
    "address": "New York such real property having a street address of",
    "month_number": "specify number of months or",
    "total_rent_for_term_words": "DOLLARS",
    "total_rent_for_term_digits": "undefined_2",
    "equal_installments_of_number": "day of each month of the term in equal installments of",
    "equal_installments_of_words": "DOLLARS_2",
    "equal_installments_of_digits": "undefined_3",
    "second_payment_words": "DOLLARS_3",
    "second_payment_digits": "receipt",
    "family_consisting_of": "USE OF PREMISES  The Premises shall be used and occupied by Tenant and Tenant",
    "owning_at_words": "DOLLARS_4",
    "owning_at_digits": "per month and except that such tenancy shall be terminable upon fifteen 15 days written",
    "late_fee_amount_words": "DOLLARS_5",
    "late_fee_amount_digits": "undefined_4",
    "landlords_name": "Landlords Name",
    "landlords_address": "1",
    "tenants_name": "Tenants Name",
    "tenants_address": "1_2",
}

# Key: meaning key for user input form
# Value:
#   `dict`(key: day|month|year, value: fillable field key in PDF file}
# | `str` (if full date in one fillable field)
new_york_residential_lease_agreement_key_map_date = {
    "date_1": {
        "day": "day of",
        "month": "20",
        "year": None
    },
    "date_2": {
        "day": "As to Landlord this",
        "month": "day of_2",
        "year": "20_2"
    },
    "date_3": {
        "day": "As to Tenant this",
        "month": "day of_3",
        "year": "20_3"
    },
    "begin_date": "and ending at 12 oclock midnight on",
    "end_date": "undefined",
    "second_payment_date": "All such payments shall be made to Landlord at Landlords address as"

}

# Meaning fill key map
new_york_residential_lease_agreement_key_map = {
    "days": "day of",
    "month": "20",
    "landlord": "hereinafter referred to as Landlord and",
    "tenant": "hereinafter referred to as Tenant",
    "city": "County",
    "address": "New York such real property having a street address of",
    "month_number": "specify number of months or",
    "begin_date": "and ending at 12 oclock midnight on",
    "end_date": "undefined",
    "total_rent_for_term_words": "DOLLARS",
    "total_rent_for_term_digits": "undefined_2",
    "equal_installments_of_number": "day of each month of the term in equal installments of",
    "equal_installments_of_words": "DOLLARS_2",
    "equal_installments_of_digits": "undefined_3",
    "second_payment_date": "All such payments shall be made to Landlord at Landlords address as",
    "second_payment_words": "DOLLARS_3",
    "second_payment_digits": "receipt",
    "family_consisting_of": "USE OF PREMISES  The Premises shall be used and occupied by Tenant and Tenant",
    "owning_at_words": "DOLLARS_4",
    "owning_at_digits": "per month and except that such tenancy shall be terminable upon fifteen 15 days written",
    "late_fee_amount_words": "DOLLARS_5",
    "late_fee_amount_digits": "undefined_4",
    "landlords_name": "Landlords Name",
    "landlords_address": "1",
    "tenants_name": "Tenants Name",
    "tenants_address": "1_2",
    "days_2": "As to Landlord this",
    "month_2": "day of_2",
    "year": "20_2",
    "day_3": "As to Tenant this",
    "month_3": "day of_3",
    "year_2": "20_3",
}


def get_value_map(_val):
    _values = None
    _default_value = None
    if isinstance(_val, dict):
        _values = _val.get('values')
        _default_value = _val.get('default')
        if not _default_value:
            if not _default_value:
                if isinstance(_values, list):
                    _default_value = _values[0]
    if isinstance(_val, list):
        _default_value = _val[0]
        _values = _val

    return _values, _default_value
