import pdfrw

ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
ANNOT_VAL_KEY = '/V'
ANNOT_RECT_KEY = '/Rect'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'


def fill_pdf_form(input_pdf, output_pdf_path, data_dict):
    """
    Fill PDF file form and write to a new PDF file
    :param input_pdf: FileStream or file path to pdf file
    :param output_pdf_path: absolute path will save output file
    :param data_dict: dict for map key value
    """
    template_pdf = pdfrw.PdfReader(input_pdf)
    for page in template_pdf.pages:
        annotations = page[ANNOT_KEY]
        for annotation in annotations:
            if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
                if annotation[ANNOT_FIELD_KEY]:
                    key = annotation[ANNOT_FIELD_KEY][1:-1]
                    if key in data_dict.keys():
                        annotation.update(
                            pdfrw.PdfDict(
                                V='{}'.format(data_dict[key]),
                                AP=''
                            )
                        )

    pdfrw.PdfWriter().write(output_pdf_path, template_pdf)


def extract_fillable_key_pdf(pdf_file_path) -> list:
    """
    Extract fillable key from pdf file, use for build keymap
    :param pdf_file_path: path to pdf file
    :return: list contains all fillable keys
    """
    template_pdf = pdfrw.PdfReader(pdf_file_path)
    keys = []
    for field in template_pdf.Root.AcroForm.Fields:
        key = field.T[1:-1]
        keys.append(key)

    return keys
