from django import forms
from django.utils.timezone import now

from pdf.models import PDFFile
from .fill_map import (
    new_york_residential_lease_agreement_key_map,
    new_york_residential_lease_agreement_map,
    get_value_map,
    new_york_residential_lease_agreement_key_map_text,
    new_york_residential_lease_agreement_key_map_date
)


class UploadPDFForm(forms.ModelForm):

    """
    Form use for upload pdf file and get custom user inputs
    """

    pdf_file_map = new_york_residential_lease_agreement_map
    text_key_map = new_york_residential_lease_agreement_key_map_text
    date_key_map = new_york_residential_lease_agreement_key_map_date

    class Meta:
        model = PDFFile
        fields = ['input_file']
        widgets = {
            'input_file': forms.FileInput(attrs={
                'class': 'form-control'
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_text_field()
        self.add_date_field()

    @property
    def date_field_keys(self):
        """
        This property use to define which fields will be treat as datetimepicker
        """
        return self.date_key_map.keys()

    def add_text_field(self):
        """
        Add text field from `new_york_residential_lease_agreement_key_map_text` to this form
        """
        for k, v in self.text_key_map.items():
            _value = self.pdf_file_map.get(v)
            _values, _default_value = get_value_map(_value)

            self.fields[k] = forms.CharField(
                # choices=[(_v, _v) for _v in _values],
                label=k,
                widget=forms.TextInput(attrs={
                    'class': 'form-control'
                }),
                initial=_default_value,
            )
    #
    # @property
    # def current_date(self):
    #     _date = now().date()
    #     return '{}/{}/{}'.format(_date.day, _date.month, _date.year)

    def add_date_field(self):
        """
        Add date field from `new_york_residential_lease_agreement_key_map_date` to this form
        """
        for k, v in self.date_key_map.items():
            # _value = self.pdf_file_map.get(v)
            # _values, _default_value = get_value_map(_value)
            self.fields[k] = forms.DateField(
                # choices=[(_v, _v) for _v in _values],
                label=k,
                widget=forms.DateInput(attrs={
                    'class': 'form-control',
                    'with': '276'
                }),
                # initial=self.current_date
            )

    def add_custom_fill_key_in_pdf_file(self):
        for k, v in new_york_residential_lease_agreement_key_map.items():
            _value = self.pdf_file_map.get(v)
            _values, _default_value = get_value_map(_value)

            self.fields[k] = forms.CharField(
                # choices=[(_v, _v) for _v in _values],
                label=k,
                widget=forms.TextInput(attrs={
                    'class': 'form-control'
                }),
                initial=_default_value,
            )

    @staticmethod
    def split_date_str(date_str):
        date = date_str.split('/')
        return date[0], date[1], date[2][:2]


    @property
    def pdf_writeable_data(self):
        """
        Generate map data from user inputs to fillable field in PDF file.
        """
        _data = {}
        for k, v in self.data.items():
            if self.text_key_map.get(k):
                _k = self.text_key_map.get(k)
                _data[_k] = v
            elif self.date_key_map.get(k):
                _k = self.date_key_map.get(k)
                if isinstance(_k, str):
                    _data[_k] = v
                elif isinstance(_k, dict):
                    _data[_k['month']], \
                    _data[_k['day']], \
                    _data[_k['year']] = self.split_date_str(v)

        return _data

    def save(self, commit=False):
        instance = super().save(commit=False)
        instance.save(custom_data=self.pdf_writeable_data)
        return instance

