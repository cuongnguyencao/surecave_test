import os
from django.db import models
from pdf.utils import fill_pdf_form
# Create your models here.
from test1.settings import MEDIA_ROOT


class PDFFile(models.Model):
    input_file = models.FileField(upload_to='input_file')
    output_file = models.FileField(upload_to='output_file', null=True)

    def save(self, custom_data=None, **kwargs):
        if not self.id:
            assert custom_data, "save PDF fle assert custom_data"
            super().save(**kwargs)
            self.generate_output_file(custom_data=custom_data)
        else:
            super().save(**kwargs)

    @staticmethod
    def build_output_file_name(input_file_name : str):
        """
        Build output file name by input file name
        """
        file_name, ext = os.path.splitext(input_file_name)
        file_name += '_output.{}'.format(ext)
        return file_name

    def generate_output_file(self, custom_data: dict):
        """
        Generate filled PDF file from user uploaded file
        """
        file = self.input_file.file
        relative_out_put_path = self.build_output_file_name(self.input_file.name)
        out_put_path = os.path.join(MEDIA_ROOT, relative_out_put_path)
        fill_pdf_form(file, out_put_path, data_dict=custom_data)
        self.output_file = relative_out_put_path
        self.save()

    def get_input_url(self):
        return self.input_file.url

    def get_file_url(self):
        if self.output_file:
            return self.output_file.url
        return self.get_input_url()

    def get_file_name(self):
        if self.output_file:
            return os.path.basename(self.output_file.name)
        else:
            return os.path.basename(self.input_file.name)

