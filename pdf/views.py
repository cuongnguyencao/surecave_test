from django.views.generic import TemplateView
from .forms import UploadPDFForm
from .models import PDFFile
# Create your views here.

class IndexView(TemplateView):
    template_name = 'home.html'
    form_class = UploadPDFForm

    @property
    def pdf_files(self):
        return PDFFile.objects.all()[::-1]

    def get_context_data(self, **kwargs):
        _context = super().get_context_data(**kwargs)
        _context['form'] = self.form_class()
        _context['files'] = self.pdf_files
        return _context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            form.save()

        return self.get(request, *args, **kwargs)